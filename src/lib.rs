pub mod base_list;
pub mod linked_list;
pub mod index;

pub use self::base_list::BaseList;
pub use self::linked_list::LinkedList;
pub use self::index::{ImmutableIndex, ForwardIndex, BackwardIndex, BidirectionalIndex};

#[cfg(test)]
mod tests {
    #[test]
    fn list_push_pop() {
        use ::LinkedList;

        let mut lst: LinkedList<i32> = LinkedList::new();
        lst.push_back(6);
        lst.push_front(7);

        let mut mul = 1;

        lst.pop_back(|&x| mul *= x);
        lst.pop_front(|&x| mul *= x);

        assert_eq!(mul, 42)
    }

    #[test]
    fn list_iter() {
        use ::LinkedList;

        let mut lst: LinkedList<i32> = LinkedList::new();

        for i in 0..7 { lst.push_back(i); }
        for i in (0..7).rev() { lst.push_front(i); }

        assert_eq!(lst.iter().sum::<i32>(), 42)
    }

    #[test]
    fn list_delete_max() {
        use ::LinkedList;
        use ::index::*;

        let mut lst: LinkedList<i32> = LinkedList::new();

        for i in (0..20).rev() {
            if i % 2 == 0 {
                lst.push_front(i)
            } else {
                lst.push_back(i);
            }
        }

        let mut idx = lst.begin();
        let mut max = 0;
        let mut max2 = 0;
        let mut max_idx = None;

        while lst.valid(idx) {
            let val = lst[idx];
            if val > max {
                max = val;
                max_idx = Some(idx);
            }
            lst.increment(&mut idx);
        }

        assert!(max_idx.is_some());

        lst.remove(max_idx.unwrap(), |x| max2 = *x);

        assert_eq!(max, 19);
        assert_eq!(max, max2);
        assert_eq!(lst.iter().sum::<i32>(), 171);
    }
}
