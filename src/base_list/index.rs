use super::{BaseList, FIRST, LAST, INVALID};
use ::{ImmutableIndex, ForwardIndex, BackwardIndex};

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct BaseListIndex
{
    pub(super) index: usize
}

impl<T> ImmutableIndex for BaseList<T>
{
    type Index = BaseListIndex;
    fn begin(&self) -> BaseListIndex { BaseListIndex { index: FIRST } }
    fn end(&self) -> BaseListIndex { BaseListIndex { index: LAST } }
    fn valid(&self, idx: BaseListIndex) -> bool { idx.index != INVALID }
}

impl<T> ForwardIndex for BaseList<T>
{
    fn increment(&self, idx: &mut BaseListIndex) { idx.index = self.next(idx.index); }
}

impl<T> BackwardIndex for BaseList<T>
{
    fn decrement(&self, idx: &mut BaseListIndex) { idx.index = self.prev(idx.index); }
}
