use ::base_list::BaseListIndex;
use ::{ImmutableIndex, ForwardIndex, BackwardIndex};

use super::LinkedList;

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct LinkedListIndex
{
    pub(super) base_index: BaseListIndex
}

impl<T> ImmutableIndex for LinkedList<T>
{
    type Index = LinkedListIndex;
    fn begin(&self) -> LinkedListIndex { LinkedListIndex { base_index: self.base_list.next(self.base_list.begin()) } } 
    fn end(&self) -> LinkedListIndex { LinkedListIndex { base_index: self.base_list.prev(self.base_list.end()) } }
    fn valid(&self, idx: LinkedListIndex) -> bool
    {
        idx.base_index != self.base_list.begin()
            && idx.base_index != self.base_list.end()
            && self.base_list.valid(idx.base_index)
    }
}

impl<T> ForwardIndex for LinkedList<T>
{
    fn increment(&self, idx: &mut LinkedListIndex) { self.base_list.increment(&mut idx.base_index) }
}

impl<T> BackwardIndex for LinkedList<T>
{
    fn decrement(&self, idx: &mut LinkedListIndex) { self.base_list.decrement(&mut idx.base_index) }
}
