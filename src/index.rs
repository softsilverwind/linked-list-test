use std::ops::{Index, IndexMut};
use std::cmp::PartialEq;
use std::fmt::Debug;
use std::collections::VecDeque;

pub trait ImmutableIndex: Index<<Self as ImmutableIndex>::Index>
                        + IndexMut<<Self as ImmutableIndex>::Index>
{
    type Index: PartialEq + Copy + Clone + Debug;
    fn begin(&self) -> Self::Index;
    fn end(&self) -> Self::Index;
    fn valid(&self, Self::Index) -> bool;
}

pub trait ForwardIndex: ImmutableIndex
{
    fn increment(&self, &mut Self::Index);
    fn next(&self, mut idx: Self::Index) -> Self::Index { self.increment(&mut idx); idx }
    // Although funny, removing it will be an API breaking change...
    // fn bicrement(&self, index: &mut Self::Index) { self.increment(index); self.increment(index) }
}

pub trait BackwardIndex: ImmutableIndex
{
    fn decrement(&self, &mut Self::Index);
    fn prev(&self, mut idx: Self::Index) -> Self::Index { self.decrement(&mut idx); idx }
}

pub trait BidirectionalIndex: ForwardIndex + BackwardIndex {}

impl<T> BidirectionalIndex for T
    where T: ForwardIndex + BackwardIndex
{
}

impl<T> ImmutableIndex for Vec<T>
{
    type Index = usize;
    fn begin(&self) -> usize { 0 }
    fn end(&self) -> usize { self.len() - 1 }
    fn valid(&self, i: usize) -> bool { i < self.len() }
}

impl<T> ForwardIndex for Vec<T>
{
    fn increment(&self, i: &mut usize) { *i += 1; }
}

impl<T> BackwardIndex for Vec<T>
{
    fn decrement(&self, i: &mut usize) { *i = i.wrapping_sub(1); }
}

impl<T> ImmutableIndex for VecDeque<T>
{
    type Index = usize;
    fn begin(&self) -> usize { 0 }
    fn end(&self) -> usize { self.len() - 1 }
    fn valid(&self, i: usize) -> bool { i < self.len() }
}

impl<T> ForwardIndex for VecDeque<T>
{
    fn increment(&self, i: &mut usize) { *i += 1; }
}

impl<T> BackwardIndex for VecDeque<T>
{
    fn decrement(&self, i: &mut usize) { *i = i.wrapping_sub(1); }
}
